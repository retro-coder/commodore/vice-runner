
"""
    Program: Runner

Remarks:

    Useful when you do not want to actually
    install the tool into the Virtual
    Environment when testing.

"""

from vicerunner.main import run

if __name__ == '__main__':
    run()
