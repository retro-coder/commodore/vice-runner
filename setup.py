"""
Copyright 2021-2024 Paul Hocker, All rights reserved. (paul@spocker.net)
"""

import vicerunner
import re 
import io

from setuptools import setup, find_packages

long_desc = "missing"

with io.open('README.md') as t_file:
    long_desc = t_file.read()

i_requires = []
t_requires = []

setup(

    name=vicerunner.__id__,
    version=vicerunner.__version__,
    description=vicerunner.__desc__,
    long_description=long_desc,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/retro/vice-runner.git',
    author='Paul Hocker',
    author_email='paul@spocker.net',
    license='MIT',
    packages=find_packages('.'),
    include_package_data=True,
    install_requires=i_requires,
    zip_safe=True,
    tests_require=t_requires,
    entry_points={
        'console_scripts': ['vr=vicerunner.main:run'],
    },
    classifiers=[
        # Picked from
        #   http://pypi.python.org/pypi?:action=list_classifiers
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: MacOS',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.4',
        'Topic :: Utilities',
        'Topic :: Games/Entertainment',
        'Environment :: Console',
    ]
)
