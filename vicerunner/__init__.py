"""-
Copyright 2021-2024 Paul Hocker, All rights reserved. (paul@spocker.net)
"""

import io


# https://manytools.org/hacker-tools/ascii-banner/

__logo__ = """
              ███                                             
             ░░░                                              
 █████ █████ ████   ██████   ██████                           
░░███ ░░███ ░░███  ███░░███ ███░░███                          
 ░███  ░███  ░███ ░███ ░░░ ░███████                           
 ░░███ ███   ░███ ░███  ███░███░░░                            
  ░░█████    █████░░██████ ░░██████                           
   ░░░░░    ░░░░░  ░░░░░░   ░░░░░░                            
 ████████  █████ ████ ████████   ████████    ██████  ████████ 
░░███░░███░░███ ░███ ░░███░░███ ░░███░░███  ███░░███░░███░░███
 ░███ ░░░  ░███ ░███  ░███ ░███  ░███ ░███ ░███████  ░███ ░░░ 
 ░███      ░███ ░███  ░███ ░███  ░███ ░███ ░███░░░   ░███     
 █████     ░░████████ ████ █████ ████ █████░░██████  █████    
░░░░░       ░░░░░░░░ ░░░░ ░░░░░ ░░░░ ░░░░░  ░░░░░░  ░░░░░     
                                                              """
__id__ = "vr"
__name__ = "Vice Runner"
__desc__ = "A Command Line Utility to Assist in using VICE with different Configurations"
__copyright__ = "Copyright 2021-2024 Paul Hocker, All rights reserved. (paul@spocker.net)"
__version__ = '0.0.1'