"""
Copyright 2021-2024 Paul Hocker, All rights reserved. (paul@spocker.net)
"""

from email.policy import strict
import logging
import string
import sys
import tempfile
import configparser
import subprocess
from unicodedata import name
import vicerunner
import os
import pathlib
import re 

parents = []
settings = []
cmd_options = ''
emulator = 'x64sc'

def _get_configurations(config):

	_configs = {}

	if (config.args.verbose): print(config.vice_config_path.split(os.pathsep))

	for dir in config.vice_config_path.strip().split(os.pathsep):

		if (len(dir) > 0):

			if (config.args.verbose): print(dir)

			_files = pathlib.Path(dir).glob('**/*.ini')
			for path in _files:

				_cfg = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation(), strict=False)

				try:
					_cfg.read(path)
					if _cfg.has_section('vice'):
						
						_name = os.path.splitext(path.name)[0]
						_path = path
						_desc = _cfg.get('vice', 'description')
						_type = ""

						if (_cfg.has_option("vice", "type")):
							_type = _cfg.get("vice", "type")

						_configs[_name] = { "path": _path, "description": _desc, "type": _type}

				except Exception as exception:

					logging.error(exception)
					logging.error('- operation cancelled.')
					sys.exit(1)

	return _configs


def search_path(name, path=None, exts=('',)):
	"""Search PATH for a binary.

	Args:
	name: the filename to search for
	path: the optional path string (default: os.environ['PATH')
	exts: optional list/tuple of extensions to try (default: ('',))

	Returns:
	The abspath to the binary or None if not found.
	"""
	path = path or os.environ('PATH')
	_splitpath = os.path.splitext(name)
	_ext = _splitpath[1]

	for dir in path.split(os.pathsep):
		
		if _ext == "":
			for ext in exts:
				binpath = os.path.join(dir, name) + "." + ext
				if os.path.exists(binpath):
					return os.path.abspath(binpath)
		else:
			binpath = os.path.join(dir, name)
			if os.path.exists(binpath):
				return os.path.abspath(binpath)

	return None
  
  
def _vars_replace(config, value):

	for _var in config.vars:
		_fn = "${%s}" % _var[0]
		_fv = _var[1]
		value = value.replace(_fn, _fv)

	return value


def _get_parents(config, parent_name):

	parents.insert(0, parent_name)
	_cfg_filename = search_path('{0}.ini'.format(parent_name), config.vice_config_path)

	if _cfg_filename:
		if (config.args.verbose): print("found parent config at {0}".format(_cfg_filename))
		_cfg = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
		_cfg.read(_cfg_filename)
		
		if (_cfg.has_option('vice', 'parent')):
			_parent_name = _cfg.get('vice', 'parent')
			_get_parents(config, _parent_name)
	

def _process_includes(config, config_name):

	if (config.args.verbose): print(config_name)

	_cfg_filename = '{0}\{1}.ini'.format(config.vice_config_path, config_name)
	_cfg_filename = search_path('{0}.ini'.format(config_name), config.vice_config_path)

	if _cfg_filename:

		if (config.args.verbose): print("found include config at {0}".format(_cfg_filename))
		_cfg = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
		_cfg.read(_cfg_filename)

		if (_cfg.has_option('vice', 'emulator')):
			config.emulator = _cfg.get('vice', 'emulator')

		if (_cfg.has_section('include')):
			_includes = _cfg.items('include', raw=True)
			for include in _includes:
				if (config.args.verbose): print(include)
				if (include[1] == '1'):
					_process_includes(config, include[0])

		if (_cfg.has_section('options')):
			_options = _cfg.items('options', raw=True)
			for option in _options:
				if (config.args.verbose): print(option)
				config.cmd_options += "{0} {1} ".format(option[0], _vars_replace(config, option[1]))

		if (_cfg.has_section('settings')):
			_settings = _cfg.items('settings', raw=True)
			for setting in _settings:
				if (config.args.verbose): print(setting)
				_o = "{0}={1}\n".format(setting[0], _vars_replace(config, setting[1])) 
				config.settings.append(_o)


def _createConfig(config, tempfile):

	config.cmd_options = ''
	config.emulator = 'x64sc'
	config.settings = []

	_tmp: bytes = []

	_cfg_filename = search_path(f'{config.args.config_name}.ini', config.vice_config_path)
	_cfg = configparser.ConfigParser(strict=False)
	_cfg.read(_cfg_filename)

	if (_cfg.has_option('vice', 'emulator')):
		config.emulator = _cfg.get('vice', 'emulator')

	config.media_path = ".;"

	if (_cfg.has_option('vice', 'media_path')):
		config.media_path += _cfg.get('vice', 'media_path')
		config.media_path = _vars_replace(config, config.media_path)

	# config.version = "3.7.1"
	# if _cfg.has_option('vice', 'config_version'):
	# 	config.version = _cfg.get('vice', 'config_version')

	_tmp.append(b'[Version]\n')

	_tmp.append(b'ConfigVersion=%b\n' % (config.vice_config_version).encode())
	_tmp.append(b'\n')


	_process_includes(config, config.args.config_name)

	# the config file must match the emu

	if (config.emulator == 'x64sc'):
		_tmp.append(b'[C64SC]\n')

	if (config.emulator == 'x128'):
		_tmp.append(b'[C128]\n')

	if (config.emulator == 'xvic'):
		_tmp.append(b'[VIC20]\n')

	for setting in config.settings:
		_tmp.append(bytes(setting, 'utf-8'))

	_extensions = ['d64', 'd71', 'd81', 'prg', 'crt', 'tap', 'vsf']
	for option in config.args.options:
		_option = search_path(option, config.media_path, _extensions)

		if (_option):
			if (config.args.verbose):
				print(f'found file option ${_option}')
			config.cmd_options += (f'"{_option}" ')

	# build the command line
	_cmd = 'start {0}\{1} -config {2} {3}'.format(
		config.vice_path, 
		config.emulator, 
		tempfile,
		config.cmd_options,
		)

	return _tmp, _cmd


def list(config):

	_configs = _get_configurations(config)
	_sorted = dict(sorted(_configs.items(), key=lambda item: item[0]))

	if (config.args.types):
		print('Available Types')
		_types = []
		for _config in _sorted:
			if (_configs[_config]['type'] != ''):
				_types.append(_configs[_config]['type'])

		_types = [*set(_types)]
		_types.sort()
		for type in _types:
			print(type)
		return
		

	print('Available Vice Configurations')
	print()
	if (config.args.verbose): print(_configs)

	_names = []
	for _config in _sorted:
		_names.append(_config)

	if (config.args.type):
		_items = []
		for _config in _names:
			if(_configs[_config]['type'] == config.args.type):
				_items.append(_config)
		_names = _items

	if (config.args.search):
		_items = []
		for _config in _names:
			if (re.search(config.args.search, _config, flags=re.IGNORECASE) or re.search(config.args.search, _configs[_config]["description"], flags=re.IGNORECASE)):
				_items.append(_config)
		_names = _items
	
	for _config in _names:
		print('{:30.40}{:10.10}{:40.40s}'.format(_config, _configs[_config]["type"], _configs[_config]["description"]))


def run(config):

	_tmpFile = tempfile.NamedTemporaryFile(prefix='vr-', delete=False)

	_config, _command = _createConfig(config, '{filename}')

	if (config.args.view_config or config.args.view):
		_str = ''
		for c in _config:
			_str += c.decode()
		print(_str)

	if (config.args.view_command or config.args.view):
		print(_command)
	
	if (config.args.view_config or config.args.view_command or config.args.view):
		return
		
	config.cmd_options = ''
	config.emulator = 'x64sc'
	config.settings = []

	_cfg_filename = search_path('{0}.ini'.format(config.args.config_name), config.vice_config_path)
	_cfg = configparser.ConfigParser(strict=False)
	_cfg.read(_cfg_filename)

	_name = ''
	_desc = 'None'

	_name = os.path.basename(_cfg_filename)
	_name = _name.split('.')[0]

	if (_cfg.has_option('vice', 'description')):
		_desc = _cfg.get('vice', 'description')

	print('> {0} : {1}'.format(_name, _desc))

	if (_cfg.has_option('vice', 'emulator')):
		config.emulator = _cfg.get('vice', 'emulator')

	config.media_path = ".;"

	if (_cfg.has_option('vice', 'media_path')):
		config.media_path += _cfg.get('vice', 'media_path')
		config.media_path = _vars_replace(config, config.media_path)


	for line in _config:
		_tmpFile.write(line)


	# replace the filename with our actual temp filename
	_command = _command.format(filename = _tmpFile.name)

	# try to run the command
	try:

		# check for a run command
		if (_cfg.has_option('vice', 'run')):
			_run = _vars_replace(config, _cfg.get('vice', 'run'))
			print(_run)
			_run_proc = subprocess.Popen(_run, shell=False, creationflags = subprocess.CREATE_NEW_CONSOLE)

		_proc = subprocess.Popen(_command, shell=True)
		_proc.communicate()

		if (config.args.verbose): print(_proc.stdout)
		if (config.args.verbose): print(_proc.stderr)
		if (config.args.verbose): print(_proc.returncode)

		sys.exit(0)

	except Exception as e:

		print(e)
		sys.exit(1)


def version(config):
	'''
	Displays the Version of this tool.
	'''
	logging.log(99, "{0} {1}".format("Version", vicerunner.__version__))

