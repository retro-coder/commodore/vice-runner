"""
Copyright 2021-2024 Paul Hocker, All rights reserved. (paul@spocker.net)
"""

import logging 
import argparse


import vicerunner.commands as commands

import sys
import configparser
import pathlib
import os
import dotmap
import vicerunner


_config = dotmap.DotMap()
_vice_path = r'.'
_vice_config_path = r'.'
_vice_snapshot_path = r'.'
_vice_config_version = r'3.7.1'
_conf_path = pathlib.Path.home()
_conf_file = "{0}\{1}".format(_conf_path, '.vicerunner')


if (os.path.isfile(_conf_file)):
	_cvr_cfg = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
	_cvr_cfg.read(_conf_file)
	_vice_path = _cvr_cfg.get('vicerunner', 'vice_path')
	_vice_config_path = _cvr_cfg.get('vicerunner', 'vice_config_path')
	_vice_snapshot_path = _cvr_cfg.get('vicerunner', 'vice_snapshot_path')
	_vice_config_version = _cvr_cfg.get('vicerunner', 'vice_config_version')
	_vars = _cvr_cfg.items('vars')
	_config.vars = _vars


def run():

	logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

	logging.log(99, vicerunner.__logo__)

	try:

		_parser = argparse.ArgumentParser(prog='vr')
		_subparser = _parser.add_subparsers(dest='command')
		_subparser.required = True

		# run command

		_cmd = _subparser.add_parser('run', help='Run Emulator With Configuration')
		_cmd.set_defaults(func=commands.run)
		_cmd.add_argument("-i", "--ignore-snapshot", help="If a Snapshot is Found, ignore using it", action='store_true')
		_cmd.add_argument('-v', '--view', help='View the Configuration and Command Without Running it', action='store_true')
		_cmd.add_argument('-c', '--view-config', help='View the Configuration Only Without Running it', action='store_true')
		_cmd.add_argument('-m', '--view-command', help='View the Command to Run Without Running it', action='store_true')
		_cmd.add_argument('--verbose', help='View the Command to Run Without Running it', action='store_true')

		_cmd.add_argument(
			'config_name',
			help='Configuration name.'
		)
		_cmd.add_argument('options', nargs='*')
		# _cmd.add_argument('options', nargs=argparse.REMAINDER)

		# list command

		_cmd = _subparser.add_parser('list', help='Show List of Runnable Configurations')
		_cmd.set_defaults(func=commands.list)
		_cmd.add_argument("search", action="store", nargs="?",
			help="Search For Configurations Matching Expression")
		_cmd.add_argument(
			"-v", "--verbose",
			help="List All Configurations", action='store_true')
		_cmd.add_argument(
			"-t", "--type",
			help="List Configurations Of A Specific Type")
		_cmd.add_argument(
			"--types", action='store_true',
			help="List Available Types")

		# version command

		_cmd = _subparser.add_parser('version', help='Show the Version')
		_cmd.set_defaults(func=commands.version)


		_args = _parser.parse_args()

		_config.args = _args 
		_config.vice_path = _vice_path
		_config.vice_config_path = _vice_config_path
		_config.vice_snapshot_path = _vice_snapshot_path
		_config.vice_config_version = _vice_config_version

		_args.func(_config)

		sys.exit(0)

	except Exception as exception:

		logging.error(exception)
		logging.error('- operation cancelled.')
		sys.exit(1)
