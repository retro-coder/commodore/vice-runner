"""
Copyright 2021-2024 Paul Hocker, All rights reserved. (paul@spocker.net)
"""

import vicerunner.main as main


if __name__ == '__main__':
    main.run()
